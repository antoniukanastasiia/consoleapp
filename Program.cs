﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Globalization;

namespace NotebookApp
{

    class Record
    {
        public string SurName { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string PhoneNumber { get; set; }
        public string Country{ get; set; }
        public DateTime BirthDate { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string OtherNotes { get; set; }
        public int ID { get; set; }
    }    
    
    class Notebook
    {
        static List<Record> records = new List<Record>();
        static int lastID = 0;
        static string systemMessage="Добро пожаловать в записную книгу!";


        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine(systemMessage);
                Console.WriteLine($"Количество записей: {records.Count}\n\n\n");
                Console.WriteLine("Выберите действие: ");
                Console.WriteLine("1-Создать новую запись.");
                Console.WriteLine("2-Редактировать запись.");
                Console.WriteLine("3-Удалить запись.");
                Console.WriteLine("4-Просмотреть созданную запись.");
                Console.WriteLine("5-Просмотреть все созданные записи в общем списке.");
                Console.WriteLine("6-Выход.");
                // получаем символ, но не выводим его
                ConsoleKeyInfo answer = Console.ReadKey(true);
                string pressedKey = answer.KeyChar.ToString();
                Record targetRecord = null;

                switch (pressedKey)
                {
                    case "1":
                        Console.Clear();
                        CreateRecord();
                        break;
                    case "2":
                        Console.Clear();
                        //обеспечиваем ввод ID записи пользователем и проверяем, существует ли он
                        //в случае верного ID возвращаем запись, иначе - null
                        ViewRecords();
                        targetRecord = GetRecordFromUser();
                        if (!(targetRecord is null))
                        {
                            EditRecord(targetRecord);
                        }

                        break;
                    case "3":
                        Console.Clear();
                        ViewRecords();
                        targetRecord = GetRecordFromUser();
                        if (!(targetRecord is null))
                        {
                            DeleteRecord(targetRecord);
                        }
                        break;
                    case "4":
                        Console.Clear();
                        ViewRecords();
                        targetRecord = GetRecordFromUser();
                        if (!(targetRecord is null))
                        {
                            ShowRecord(targetRecord);
                            Console.WriteLine("\nНажмите любую клавишу для продолжения...");
                            Console.ReadKey(true);
                        }
                        break;
                    case "5":
                        Console.Clear();
                        ViewRecords();
                        Console.WriteLine("\nНажмите любую клавишу для продолжения...");
                        Console.ReadKey(true);
                        break;
                    case "6":
                        Environment.Exit(0);
                        break;
                    default:
                        break;

                }
            }
        }

        static void CreateRecord()
        {
            Record record = new Record();
            Console.WriteLine("Создание новой записи!\n");
            Console.Write("Фамилия: ");
            record.SurName = Console.ReadLine();
            while (true)
            {
                //проверяем на предмет пустой записи (обязательное поле)
                if (record.SurName.Length == 0)
                {
                    Console.WriteLine("Обязательное поле!");
                    Console.Write("Фамилия: ");
                    record.SurName = Console.ReadLine();
                }
                else
                {
                    break;
                }
            }
            Console.Write("Имя: ");
            record.Name = Console.ReadLine();
            while (true)
            {
                //проверяем на предмет пустой записи (обязательное поле)
                if (record.Name.Length == 0)
                {
                    Console.WriteLine("Обязательное поле!");
                    Console.Write("Имя: ");
                    record.Name = Console.ReadLine();
                }
                else
                {
                    break;
                }
            }
            Console.Write("Отчество(опционально): ");
            record.SecondName = Console.ReadLine();
            Console.Write("Номер телефона(только цифры): ");
            //допускаем ввод только символов
            record.PhoneNumber = LimitInput("0123456789");
            while (true)
            {
                //проверяем на предмет пустой записи (обязательное поле)
                if (record.PhoneNumber.Length == 0)
                {
                    Console.WriteLine("Обязательное поле!");
                    Console.Write("Номер телефона(только цифры): ");
                    record.PhoneNumber = LimitInput("0123456789");
                }
                else
                {
                    break;
                }
            }
            Console.Write("\nСтрана: ");
            record.Country = Console.ReadLine();
            while (true)
            {
                //проверяем на предмет пустой записи (обязательное поле)
                if (record.Country.Length == 0)
                {
                    Console.WriteLine("Обязательное поле!");
                    Console.Write("Страна: ");
                    record.Country = Console.ReadLine();
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                Console.Write("День Рождения(опционально, дд.мм.гггг): ");
                //допускаем ввод только символов и точки
                string birthDate = LimitInput("0123456789.");
                //если ничего не вводим, то присваивается значение по умолчанию
                if(birthDate.Length==0)
                {
                    record.BirthDate = DateTime.MinValue;
                    break;
                }
                //парсим дату, формат должен в точности совпадать 
                else if (DateTime.TryParseExact(birthDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
                {
                    record.BirthDate = date;
                    break;
                }
                else
                {
                    Console.WriteLine("\nОшибка ввода даты!");

                }
            }
            Console.Write("\nОрганизация(опционально): ");
            record.Company = Console.ReadLine();
            Console.Write("Должность(опционально): ");
            record.Position = Console.ReadLine();
            Console.Write("Прочие заметки(опционально): ");
            record.OtherNotes = Console.ReadLine();
            lastID++;
            record.ID = lastID;
            records.Add(record);
            systemMessage = $"Вы успешно добавили запись в записную книгу ({record.Name} {record.SurName}, ID: {record.ID})";
        }

        static void ViewRecords()
        {
            Console.Clear();
            foreach(Record record in records)
            {
                Console.WriteLine(String.Format("{0,-5}{1,-15}{2,-15}{3,-12}", "ID", "Фамилия", "Имя", "Номер телефона"));
                Console.WriteLine(String.Format("{0,-5}{1,-15}{2,-15}{3,-12}", record.ID, record.SurName, record.Name, record.PhoneNumber));
            }
        }

        static void ShowRecord(Record targetRecord)
        {

            Console.WriteLine("Фамилия: " + targetRecord.SurName);
            Console.WriteLine("Имя: " + targetRecord.Name);
            Console.WriteLine("Отчество: " + targetRecord.SecondName);
            Console.WriteLine("Номер телефона: " + targetRecord.PhoneNumber);
            Console.WriteLine("Страна: " + targetRecord.Country);
            if(targetRecord.BirthDate==DateTime.MinValue)
            {
                Console.WriteLine("День Рождения:");
            }
            else
            {
                Console.WriteLine("День Рождения: " + targetRecord.BirthDate.ToString("dd.MM.yyyy"));
            }
            Console.WriteLine("Организация: " + targetRecord.Company);
            Console.WriteLine("Должность: " + targetRecord.Position);
            Console.WriteLine("Прочие заметки: " + targetRecord.OtherNotes);
            systemMessage = $"Последняя просмотренная запись: {targetRecord.Name} {targetRecord.SurName}, ID: {targetRecord.ID}";
        }
        static void DeleteRecord(Record targetRecord)
        {
            ShowRecord(targetRecord);
            Console.WriteLine("\nВы действительно хотите удалить запись? Д/н");
            while (true)
            {
                ConsoleKeyInfo symbol = Console.ReadKey(true);
                string pressedKey = symbol.KeyChar.ToString();
                if (pressedKey == "Д")
                {
                    records.Remove(targetRecord);
                    systemMessage = $"Вы успешно удалили запись: {targetRecord.Name} {targetRecord.SurName}, ID: {targetRecord.ID}";
                    return;
                }
                else if (pressedKey == "н")
                {
                    systemMessage = $"Вы решили не удалять запись: {targetRecord.Name} {targetRecord.SurName}, ID: {targetRecord.ID}";
                    return;
                }
                else
                {
                    Console.WriteLine("Допустимые варианты ввода: Д/н!");
                }
            }
        }
        static void EditRecord(Record record)
        {
            Console.Write("Фамилия: ");
            // используем для редактирования значения по умолчанию
            SendKeys.SendWait(record.SurName);
            record.SurName=Console.ReadLine();
            Console.Write("Имя: ");
            SendKeys.SendWait(record.Name);
            record.Name=Console.ReadLine();
            Console.Write("Отчество(опционально): ");
            SendKeys.SendWait(record.SecondName);
            record.SecondName=Console.ReadLine();
            Console.Write("Номер телефона(только цифры): ");
            SendKeys.SendWait(record.PhoneNumber);
            record.PhoneNumber = LimitInput("0123456789");
            Console.Write("\nСтрана: ");
            SendKeys.SendWait(record.Country);
            record.Country=Console.ReadLine();
            while (true)
            {
                Console.Write("День Рождения(опционально, дд.мм.гггг): ");
                SendKeys.SendWait(record.BirthDate.ToString("dd.MM.yyyy"));
                // ограничиваем ввод числами и точкой, проверяем валидность даты
                string birthDate = LimitInput("0123456789.");
                if (DateTime.TryParseExact(birthDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
                {
                    record.BirthDate = date;
                    break;
                }
                else
                {
                    Console.WriteLine("\nОшибка ввода даты!");

                }
            }
            Console.Write("\nОрганизация(опционально): ");
            SendKeys.SendWait(record.Company);
            record.Company=Console.ReadLine();
            Console.Write("Должность(опционально): ");
            SendKeys.SendWait(record.Position);
            record.Position=Console.ReadLine();
            Console.Write("Прочие заметки(опционально): ");
            SendKeys.SendWait(record.OtherNotes);
            record.OtherNotes=Console.ReadLine();
            systemMessage = $"Вы отредактировали запись: {record.Name} {record.SurName}, ID: {record.ID}";
        }
        // метод возвращает запись по указанному id
        // до вызова метода id должен быть проверен (являться значением типа int)
        // возвращает запись или null, если запись не найдена
        static Record GetRecordFromID(int id)
        {
            Record targetRecord = null;
            foreach(Record record in records)
            {
                if (record.ID==id)
                {
                    targetRecord = record;
                    return record;
                }
            }
            return null;
        }

        // метод получает ID записи от пользователя и проверяет его на валидность
        // в случае успеха возвращает запись, при неудаче возвращает null
        static Record GetRecordFromUser()
        {
            while (true)
            {
                Console.Write("\nВведите ID записи: ");
                string inputString = Console.ReadLine();
                Console.Clear();
                // если пользователь не ввел ID
                if (inputString.Length==0)
                {
                    return null;
                }
                // проверка на числовое значение
                bool ifInt = int.TryParse(inputString, out int id);
                if (ifInt)
                {
                    // проверка на присутствие данного id в списке записей
                    Record targetRecord = GetRecordFromID(id);
                    if (!(targetRecord is null))
                    {
                        return targetRecord;
                    }
                    else

                    {
                        Console.WriteLine($"Запись с ID {id} не была найдена. Пожалуйста, повторите попытку или нажмите ВВОД для выхода");
                    }
                }
                else
                {
                    Console.WriteLine("Введенная строка не является ID. Пожалуйста, повторите попытку или нажмите ВВОД для выхода");
                }
            }
        }

        // метод ограничивает ввод символами, представленными в параметре allowedSymbols
        // возвращает введенную с ограничениями строку
        static string LimitInput(string allowedSymbols)
        {
            string returnSymbols = "";
            while (true)
            {
                // нажатую клавишу в начале не печатаем, а проверяем на соответствие разрешенному списку
                // если в списке, то добавляем к сроке для вывода
                ConsoleKeyInfo symbol = Console.ReadKey(true);
                string pressedKey = symbol.KeyChar.ToString();
                if (allowedSymbols.Contains(pressedKey))
                {
                    Console.Write(pressedKey);
                    returnSymbols += pressedKey;
                }
                // если нажимается backspace, то удаляется последний символ
                if (pressedKey == "\b")
                {
                    Console.Write(pressedKey + " \b");
                    returnSymbols = returnSymbols.Remove(returnSymbols.Length - 1);
                }
                // выходим при нажатии на ввод
                if (pressedKey == "\r")
                {
                    return returnSymbols;
                }
            }
        }

    }
}
